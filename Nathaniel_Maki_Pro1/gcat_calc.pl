use Getopt::Std;

use strict;
use warnings;

my %opt=();
getopts("i:w:s:o:t:",\%opt);


################################################################

#obtains window size

################################################################

my @ARGV || die "No input file selected";

my ($window, $key, $position, $init, $size);

while (my $line = <STDIN>) {
    if (my $line =~  /^>(.+?)\s/){
	$key = $1;
	$position = $size = 0;
	undef $init;
	    next;
    }

    chomp ($line);

    for (split //) {
	next unless $line =~ /\W/;
	$init //= $position;
	$size++;
	if ($key && $size == $window) {
	    printf "%-6s %4d %4d\n", $key, $init, $position +1;
	    undef $init;
	    $size = 0;
	}
    }
    continue {
	$position++;
    }
}

#################################################################

#AT & GC Content Portion

#################################################################

open (INPUT, "opt{o}") || die "Unable to open input file $opt{o}";
open (OUTPUT, "+>opt{z}") || die "Unable to open input file $opt{z}";

my @gcskew = &gcskew($gb, $window);
my @atskew = &atskew($gb, $window

########################################

#Calculates gcskew and determines window

########################################
my @location = ();
my $i -0;

for ($i = 0; $i * $windows < length($gb->{SEQ});$i++){
    my $seq = substr($gb->{SEQ}, $i * $window, $window);
    my $c = $seq =~ tr/c/c/;
    my $g = $seq =~ tr/g/g/;
    my $skew = ($g-$c)/($g+$c);
    push (@location, $i *window);
    push (@gcskew, $skew);
}

########################################

#Calculates atskew and determines window

########################################

for ($i = 0; $i * $windows < length($gb->{SEQ});$i++){
    my $seq = substr($gb->{SEQ}, $i * $window, $window);
    my $a = $seq =~ tr/a/a/;
    my $t = $seq =~ tr/t/t/;
    my $skew = ($a-$t)/($a+$t);
    push (@location, $i * $window);
    push (@atskew, $skew);
}
