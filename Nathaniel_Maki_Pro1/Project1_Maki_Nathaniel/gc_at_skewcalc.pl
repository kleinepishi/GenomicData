use Getopt::Std;
use strict;
use warnings;

my %opt=();
getopts("i:w:s:o:x:",\%opt);

	#
	my $gene = undef;
	my $sequence = undef;
	my $win = $opt{w};
	my $stp = $opt{s};
	my $Acon = 0;
	my $Tcon = 0;
	my $Gcon = 0;
	my $Ccon = 0;
	my $GCskw = 0;
	my $ATskw = 0;

	open (my $INPUT, "$opt{i}") || die "Unable to open input file $opt{i}";
	open (my $OUTPUT, "+>$opt{o}") || die "Unable to open output file 1 $opt{o1}";

	print $OUTPUT "Input File\t$opt{i}\nWindow Size\t$win nt\nStep Size\t$stp nt\n\nTranscript_Name\twin_Start\tGC_skw\tAT_skw\n\n";

	sub Skw {
	    my $size = length $sequence;
	    for (my $i = 0; $i < ($size - $win)/$stp; $i++) {
		$Acon = 0;
		$Tcon = 0;
		$Gcon = 0;
		$Ccon = 0;
		$GCskw = 0;
		$ATskw = 0;
		my $init = $i * $stp;
		my $window = substr($sequence, $init, $win);
		my @sequence = split("", $window);
    for (my $j = 0; $j < @sequence; $j++) {
		    if     ($sequence[$j] eq "A") {  $Acon++;
		    }elsif ($sequence[$j] eq "T") {  $Tcon++;
		    }elsif ($sequence[$j] eq "G") {  $Gcon++;
		    }elsif ($sequence[$j] eq "C") {  $Ccon++;
		    }
		}
		if ($Gcon != 0 || $Ccon !=0) {
		$GCskw = ($Gcon - $Ccon) / ($Gcon + $Ccon);
		}
		if ($Acon != 0 || $Tcon != 0) {
		$ATskw = ($Acon - $Tcon) / ($Acon + $Tcon);
		}
		my $act = $init + 1;
		print $OUTPUT "$gene\t$act\t$GCskw\t$ATskw\n";
	    }
	}

	while (my $line = <$INPUT>) {
	    if (my $line =~ />/) {
		Skw($line);
		chomp($line);
		my $length = length $line;
		my $gene = substr($line, 1, $length - 1);
		$sequence = undef;
	    }
	    else {
		my $sequence = "$seq$line";
	    }
	}
	close($INPUT);
	close($OUTPUT);

	open ($INPUT, "$opt{o}") || die "Unable to open input file $opt{o}";
	open ($OUTPUT, "+>$opt{x}") || die "Unable to open input file $opt{x}";

	my @GCtot = ();
	my @ATtot = ();
	my @amnt = ();
	my $maximum = 0;
	my $chk = 0;
	my $spt = 0;

	while (<$INPUT>) {
	    $chk++;
	    if ($chk >= 7) {
		chomp();
		my @line = split("\t",$_);
		if (@line[1] == 1) {
		    $spt = 0;
		}
		if (@line[1] > $maximum) {
		    $maximum = @line[1];
		    push(@GCtot, @line[2]);
		    push(@ATtot, @line[3]);
		    push(@amnt, 1);
			}
		else {
		    @GCtot[$spt] += @line[2];
		    @ATtot[$spt] += @line[3];
		    @amnt[$spt]++;
		}
		    $spt++;
		}
	    }

	    print $OUTPUT "Position\tAverage GC Skew\tAverage AT Skew\n\n";

	    for (my $i = 0; $i < @GCtot; $i++) {
		my $avg = @GCtot[$i]/@amnt[$i];
		my $plc = $stp * $i + 1;
		print $OUTPUT "$plc\t$avg\t";
		$avg = @ATtot[$i]/@amnt[$i];
		print $OUTPUT "$avg\n";
	    }

	    close($INPUT);
	    close($OUTPUT);
